# iDroid




## Usage
iDroid is a music player for the Steam Deck, activated by the side tab.
iDroid works by utilizing the remote debugging capabilities of CEF. It will fetch all the tabs, at present, loaded, and connect to their debug endpoints via websocket; and then evaluates js loaded from an external file. You'll be able to place and replace content on the various UI elements, without modifying any files directly from SteamOS.



## Installation
Run all these in `/home/deck`  
  
```

cd steamdeck-ui-inject

sudo steamos-readonly disable

curl https://bootstrap.pypa.io/get-pip.py > get-pip.py
sudo python get-pip.py
sudo python -m pip install -r requirements.txt

sudo ./patch-gamescope-session.sh
sudo cp devtools-inject.service /etc/systemd/system/
sudo systemctl enable --now devtools-inject

sudo steamos-readonly enable
```

Enjoy!

~M




